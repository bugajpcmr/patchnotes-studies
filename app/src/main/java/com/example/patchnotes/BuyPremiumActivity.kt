package com.example.patchnotes

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.GsonBuilder
import com.paypal.android.sdk.payments.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_buy_premium.*
import kotlinx.android.synthetic.main.comment_row.view.*
import okhttp3.*
import org.json.JSONException
import java.io.IOException
import java.math.BigDecimal

val PAYPAL_REQUEST_CODE: Int = 7171

val PAYPAL_CLIENT_ID: String = "AYq5sjseiP4hRABhoZonU__2hghagluKdXDrlvP9ElrqPJ2EeYuJcOfMk2EQcQ3q64O61Ef3xwAieH4f"
val config = PayPalConfiguration()

    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
    // or live (ENVIRONMENT_PRODUCTION)
    .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)

    .clientId(PAYPAL_CLIENT_ID)

class BuyPremiumActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_premium)



        pay_buy_premium_button.setOnClickListener {
            processPayment()
        }
    }

    fun processPayment()
    {
        val payment = PayPalPayment(
            BigDecimal("5.75"), "USD", "Premium",
            PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(this, PaymentActivity::class.java)

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)

        startActivityForResult(intent, 0)
    }

    private fun addSub(id: String) {
        val db = Firebase.firestore

        db.collection("users").whereEqualTo("id", id)
            .get()
            .addOnSuccessListener { result ->

                //val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("SinglePostActivity", "${document.id} => ${document.data}")
                    val user = User(
                        document.getString("id")!!,
                        document.getString("username")!!,
                        document.getString("email")!!,
                        document.getString("profileImageUrl")!!,
                        document.getString("sub")!!,
                        document.getString("githubLink")!!
                    )
                    if(user.id != "") {
                        db.collection("users").document(document.id)
                            .update("sub", "1")
                    }
                }
            }
            .addOnFailureListener { exception ->
                Log.w("SinglePostActivity", "Error getting documents.", exception)
            }

        db.collection("users").whereEqualTo("id", id)
            .get()
            .addOnSuccessListener { Log.d("payment", "DocumentSnapshot successfully updated!") }
            .addOnFailureListener { e -> Log.w("payment", "Error updating document", e) }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val confirm =
                data!!.getParcelableExtra<PaymentConfirmation>(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4))

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details

                    // ADD SUB TO DATABASE
                    //val sharedPreferences = getSharedPreferences("user_info", Context.MODE_PRIVATE)
                    //val userInfoId = sharedPreferences.getInt("ID", 0)

                    val uid = FirebaseAuth.getInstance().uid
                    addSub(uid!!)

                    //val editor = sharedPreferences.edit()
                    //editor.remove("SUB")
                    //editor.putInt("SUB", 1)
                    //editor.apply()

                    Toast.makeText(this, "Payment Complete", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, ProfileActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

                } catch (e: JSONException) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e)
                }

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.")
            Toast.makeText(this, "Payment Canceled", Toast.LENGTH_SHORT).show()
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i(
                "paymentExample",
                "An invalid Payment or PayPalConfiguration was submitted. Please see the docs."
            )
            Toast.makeText(this, "Payment Failed", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onDestroy() {
        stopService(Intent(this, PayPalService::class.java))
        super.onDestroy()
    }
}