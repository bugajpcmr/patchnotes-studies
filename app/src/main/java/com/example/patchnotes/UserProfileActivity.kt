package com.example.patchnotes

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.category_row.*
import kotlinx.android.synthetic.main.category_row.view.*
import kotlinx.android.synthetic.main.category_row.view.add_to_favorites_button

class UserProfileActivity : AppCompatActivity() {

    var isSub: String = "0"

    var githubURL = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        supportActionBar?.title = "Profile"

        //get id
        val uid = intent.getStringExtra("user_profile_id")!!

        profile_ispremium_textview.isVisible = false


        verifyUserIsLoggedIn()

        val adapter = GroupAdapter<GroupieViewHolder>()
        profile_recyclerview.adapter = adapter

        fetchUserData(uid)
        fetchCategoryData(uid)


        profile_email_button.setOnClickListener {
            if(githubURL != ""){
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(githubURL))
                startActivity(i)
            }
        }

        profile_home_button.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
    private fun fetchUserData(uid: String){
        val db = Firebase.firestore
        db.collection("users").whereEqualTo("id",uid)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("ProfileActivity", "${document.id} => ${document.data}")
                    val user = User(
                        document.getString("id")!!,
                        document.getString("username")!!,
                        document.getString("email")!!,
                        document.getString("profileImageUrl")!!,
                        document.getString("sub")!!,
                        document.getString("githubLink")!!
                    )
                    if(user.id != "") {
                        isSub = user.sub
                        if(isSub == "0")
                        {
                            profile_ispremium_textview.isVisible = true
                            profile_ispremium_textview.text = "NON-PREMIUM"
                        }else if(isSub == "1")
                        {
                            profile_ispremium_textview.isVisible = true
                            profile_ispremium_textview.text = "PREMIUM"
                        }
                        profile_username_textview.text = user.username
                        githubURL = user.githubLink
                        Picasso.get().load(user.profileImageUrl).into(profile_picture_imageview)
                    }
                }
            }
            .addOnFailureListener { exception ->
                Log.w("HomeActivity", "Error getting documents.", exception)
            }
    }

    private fun verifyUserIsLoggedIn() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId){

            R.id.menu_sign_out ->{

                val builder = AlertDialog.Builder(this)

                builder.setTitle("Sign out")
                builder.setMessage("Are you sure you want to sign out?")

                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    // Sign out
                    FirebaseAuth.getInstance().signOut()

                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

                    Toast.makeText(this, "Signed out", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                })

                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    // Do nothing
                    dialog.dismiss()
                })

                val alert = builder.create()
                alert.show()


            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.nav_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun fetchCategoryData(uid: String){
        val db = Firebase.firestore
        db.collection("categories").whereEqualTo("user_id", uid)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("SearchActivity", "${document.id} => ${document.data}")
                    val category = Category(
                        document.id,
                        document.getString("user_id")!!,
                        document.getString("name")!!,
                        document.getString("description")!!,
                        document.getString("imageUrl")!!,
                        document.getString("date")!!
                    )
                    if(category.id != "") {
                        adapter.add(CategoryItem(category, adapter))
                    }
                }

                adapter.setOnItemClickListener { item, view ->
                    val categoryItem = item as UserProfileActivity.CategoryItem
                    val intent = Intent(view.context, PostsActivity::class.java)
                    intent.putExtra("category_id", categoryItem.category.id)
                    intent.putExtra("user_id", categoryItem.category.user_id)
                    intent.putExtra("category_description", categoryItem.category.description)
                    intent.putExtra("category_imageurl", categoryItem.category.imageUrl)
                    intent.putExtra("category_name", categoryItem.category.name)
                    startActivity(intent)
                }

                profile_recyclerview.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("SearchActivity", "Error getting documents.", exception)
            }
    }

    class CategoryItem(val category: Category, private val adapter: GroupAdapter<GroupieViewHolder>): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.add_to_favorites_button.setBackgroundResource(R.drawable.ic_baseline_delete_forever_24)
            //viewHolder.itemView.add_to_favorites_button.isVisible = false
            viewHolder.itemView.add_to_favorites_button.isVisible = false
            viewHolder.itemView.profile_picture_category_imageview.isVisible = false
            viewHolder.itemView.home_category_name_textview.text = category.name
            viewHolder.itemView.home_category_description_textview.text = category.description
            Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)
        }
        override fun getLayout(): Int {
            return R.layout.category_row
        }
    }
}
