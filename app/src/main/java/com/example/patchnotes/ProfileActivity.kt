package com.example.patchnotes

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.category_row.*
import kotlinx.android.synthetic.main.category_row.view.*
import kotlinx.android.synthetic.main.category_row.view.add_to_favorites_button

class ProfileActivity : AppCompatActivity() {

    var isSub: String = "0"

    var githubURL = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        supportActionBar?.title = "Profile"


        profile_ispremium_textview.isVisible = false


        verifyUserIsLoggedIn()

        val adapter = GroupAdapter<GroupieViewHolder>()
        profile_recyclerview.adapter = adapter

        fetchUserData()
        fetchCategoryData()

        profile_settings_button.setOnClickListener {
            val intent1 = Intent(this, ProfileSettingsActivity::class.java)
            //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent1)
        }

        profile_email_button.setOnClickListener {
            if(githubURL != ""){
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(githubURL))
                startActivity(i)
            }
        }

        add_category_profile_button.setOnClickListener {
            if(isSub == "1")
            {
                val intent1 = Intent(this, AddCategoryActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent1)
            }else if(isSub == "0")
            {
                val intent2 = Intent(this, BuyPremiumActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)


                val builder = AlertDialog.Builder(this)

                builder.setTitle("Buy Premium")
                builder.setMessage("You need to buy premium to add projects - $5.75")

                builder.setPositiveButton("Buy Premium", DialogInterface.OnClickListener { dialog, which ->
                    // buy premium

                    startActivity(intent2)
                    finish()

                    Toast.makeText(this, "Check out with Paypal", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                })

                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    // Do nothing
                    dialog.dismiss()
                })

                val alert = builder.create()
                alert.show()
            }
        }

        profile_home_button.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
    private fun fetchUserData(){
        val uid = FirebaseAuth.getInstance().uid
        val db = Firebase.firestore
        db.collection("users").whereEqualTo("id",uid)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("ProfileActivity", "${document.id} => ${document.data}")
                    val user = User(
                        document.getString("id")!!,
                        document.getString("username")!!,
                        document.getString("email")!!,
                        document.getString("profileImageUrl")!!,
                        document.getString("sub")!!,
                        document.getString("githubLink")!!
                    )
                    if(user.id != "") {
                        isSub = user.sub
                        if(isSub == "0")
                        {
                            profile_ispremium_textview.isVisible = true
                            profile_ispremium_textview.text = "NON-PREMIUM"
                        }else if(isSub == "1")
                        {
                            profile_ispremium_textview.isVisible = true
                            profile_ispremium_textview.text = "PREMIUM"
                        }
                        profile_username_textview.text = user.username
                        githubURL = user.githubLink
                        Picasso.get().load(user.profileImageUrl).into(profile_picture_imageview)
                    }
                }
            }
            .addOnFailureListener { exception ->
                Log.w("HomeActivity", "Error getting documents.", exception)
            }
    }

    private fun verifyUserIsLoggedIn() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId){

            R.id.menu_sign_out ->{

                val builder = AlertDialog.Builder(this)

                builder.setTitle("Sign out")
                builder.setMessage("Are you sure you want to sign out?")

                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    // Sign out
                    FirebaseAuth.getInstance().signOut()

                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

                    Toast.makeText(this, "Signed out", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                })

                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    // Do nothing
                    dialog.dismiss()
                })

                val alert = builder.create()
                alert.show()


            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.nav_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun fetchCategoryData(){
        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().uid
        db.collection("categories").whereEqualTo("user_id", uid)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("SearchActivity", "${document.id} => ${document.data}")
                    val category = Category(
                        document.id,
                        document.getString("user_id")!!,
                        document.getString("name")!!,
                        document.getString("description")!!,
                        document.getString("imageUrl")!!,
                        document.getString("date")!!
                    )
                    if(category.id != "") {
                        adapter.add(CategoryItem(category, adapter))
                    }
                }

                adapter.setOnItemClickListener { item, view ->

                    val categoryItem = item as CategoryItem
                    val intent = Intent(view.context, AddPostActivity::class.java)
                    intent.putExtra("category_id", categoryItem.category.id)
                    startActivity(intent)
                }

                profile_recyclerview.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("SearchActivity", "Error getting documents.", exception)
            }
    }

    class CategoryItem(val category: Category, private val adapter: GroupAdapter<GroupieViewHolder>): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.add_to_favorites_button.setBackgroundResource(R.drawable.ic_baseline_delete_forever_24)
            //viewHolder.itemView.add_to_favorites_button.isVisible = false
            viewHolder.itemView.add_to_favorites_button.setOnClickListener {
                val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener

                val builder = AlertDialog.Builder(viewHolder.itemView.context)

                builder.setTitle("Delete")
                builder.setMessage("Are you sure you want to delete this Project?")

                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    val db = Firebase.firestore
                    db.collection("categories").document(category.id)
                        .delete()
                        .addOnSuccessListener { Log.d("ProfileActivity", "DocumentSnapshot successfully deleted!") }
                        .addOnFailureListener { e -> Log.w("ProfileActivity", "Error deleting document", e) }
                    Toast.makeText(viewHolder.itemView.context, "Project deleted", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                    adapter.clear()
                    val intent = Intent(viewHolder.itemView.context, ProfileActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    viewHolder.itemView.context.startActivity(intent)
                })

                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                    // Do nothing
                    dialog.dismiss()
                })

                val alert = builder.create()
                alert.show()

            }
            viewHolder.itemView.home_category_name_textview.text = category.name
            viewHolder.itemView.home_category_description_textview.text = category.description
            Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)
        }
        override fun getLayout(): Int {
            return R.layout.category_row
        }
    }
}
