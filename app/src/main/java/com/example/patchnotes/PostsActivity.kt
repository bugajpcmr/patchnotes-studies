package com.example.patchnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_posts.*
import kotlinx.android.synthetic.main.activity_single_post.*
import kotlinx.android.synthetic.main.category_row.view.*
import kotlinx.android.synthetic.main.post_row.view.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PostsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)

        supportActionBar?.title = "Latests posts"

        val adapter = GroupAdapter<GroupieViewHolder>()
        posts_recyclerview.adapter = adapter

        val categoryId: String = intent.getStringExtra("category_id")
        val userId: String = intent.getStringExtra("user_id")
        val categoryDescription: String = intent.getStringExtra("category_description")
        val categoryImageUrl: String = intent.getStringExtra("category_imageurl")
        val categoryName: String = intent.getStringExtra("category_name")

        profile_picture_category_imageview.setOnClickListener {
            val intent1 = Intent(this, UserProfileActivity::class.java)
            //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent1.putExtra("user_profile_id", userId)
            startActivity(intent1)
        }

        top_profile_layout.setOnClickListener {
            category_posts_description_textview.isVisible = !category_posts_description_textview.isVisible
        }

        Picasso.get().load(categoryImageUrl).into(posts_category_image_imageview)
        posts_category_name_textview.text = categoryName
        category_posts_description_textview.text = categoryDescription

        fetchPostData(categoryId)
    }


    private fun fetchPostData(category_id: String){
        val db = Firebase.firestore
        db.collection("posts").whereEqualTo("category_id", category_id)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                db.collection("posts").orderBy("date", com.google.firebase.firestore.Query.Direction.ASCENDING)
                    .get()
                for (document in result) {
                    Log.d("PostsActivity", "${document.id} => ${document.data}")
                    val post = Post(
                        document.id,
                        document.getString("category_id")!!,
                        document.getString("title")!!,
                        document.getString("description")!!,
                        document.getString("upvotes")!!,
                        document.getString("downvotes")!!,
                        document.getString("date")!!
                    )
                    if(post.id != "") {
                        adapter.add(PostItem(post))
                    }
                }

                adapter.setOnItemClickListener { item, view ->
                    val postItem = item as PostsActivity.PostItem
                    val intent = Intent(view.context, SinglePostActivity::class.java)
                    intent.putExtra("post_id", postItem.post.id)
                    intent.putExtra("post_title", postItem.post.title)
                    intent.putExtra("post_description", postItem.post.description)
                    intent.putExtra("post_date", postItem.post.date)
                    startActivity(intent)
                }

                posts_recyclerview.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("PostsActivity", "Error getting documents.", exception)
            }
    }

    class PostItem(val post: Post): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.post_title_textview.text = post.title
            viewHolder.itemView.post_description_textview.text = post.description
            viewHolder.itemView.post_date_textview.text = post.date
            //Picasso.get().load(post.imageUrl).into(viewHolder.itemView.category_home_imageview)
        }
        override fun getLayout(): Int {
            return R.layout.post_row
        }
    }
}