package com.example.patchnotes

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.category_row.view.*

class SearchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        supportActionBar?.title = "Search"

        val adapter = GroupAdapter<GroupieViewHolder>()
        recyclerview_search_categories.adapter = adapter

        search_categories_button.setOnClickListener {

            hideKeyboard()
            val categoryName = search_categories_edittext.text.toString()
            if(categoryName.isEmpty()) {
                Toast.makeText(baseContext, "Please fill the search box",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            fetchData(categoryName)
        }

    }
    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    private fun fetchData(category_name: String){
        val db = Firebase.firestore
        db.collection("categories").whereGreaterThanOrEqualTo("name", category_name)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("SearchActivity", "${document.id} => ${document.data}")
                    val category = Category(
                        document.id,
                        document.getString("user_id")!!,
                        document.getString("name")!!,
                        document.getString("description")!!,
                        document.getString("imageUrl")!!,
                        document.getString("date")!!
                    )
                    if(category.id != "") {
                        adapter.add(SearchCategoryItem(category))
                    }
                }
                adapter.setOnItemClickListener { item, view ->
                    val categoryItem = item as SearchActivity.SearchCategoryItem
                    val intent = Intent(view.context, PostsActivity::class.java)
                    intent.putExtra("category_id", categoryItem.category.id)
                    intent.putExtra("user_id", categoryItem.category.user_id)
                    intent.putExtra("category_description", categoryItem.category.description)
                    intent.putExtra("category_imageurl", categoryItem.category.imageUrl)
                    intent.putExtra("category_name", categoryItem.category.name)
                    startActivity(intent)
                }

                recyclerview_search_categories.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("SearchActivity", "Error getting documents.", exception)
            }
    }

    class SearchCategoryItem(val category: Category): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.home_category_name_textview.text = category.name
            viewHolder.itemView.add_to_favorites_button.isVisible = false
            viewHolder.itemView.home_category_description_textview.text = category.description
            Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)
            viewHolder.itemView.profile_picture_category_imageview.setOnClickListener {
                val intent1 = Intent(viewHolder.itemView.context, UserProfileActivity::class.java)
                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent1.putExtra("user_profile_id", category.user_id)
                viewHolder.itemView.context.startActivity(intent1)
            }

        }
        override fun getLayout(): Int {
            return R.layout.category_row
        }
    }
}