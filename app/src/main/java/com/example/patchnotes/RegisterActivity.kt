package com.example.patchnotes

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        supportActionBar?.title = "Sign up"

        register_button.setOnClickListener {
            performRegister()

        }

        already_have_textview.setOnClickListener {
            Log.d("RegisterActivity", "login acivity")

            //Go to Login Activity
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        select_photo_button.setOnClickListener {
            Log.d("RegisterActivity", "Show photo")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            //selected photo
            Log.d("RegisterActivity", "Photo was selected")

            selectedPhotoUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)
            //val bitmapDrawable = BitmapDrawable(bitmap)
            //select_photo_button.setBackgroundDrawable(bitmapDrawable)

            select_photo_imageview.setImageBitmap(bitmap)
            select_photo_button.alpha = 0f
        }
    }

    private fun performRegister() {
        val email = email_edittext_register.text.toString()
        val username = username_edittext_register.text.toString()
        val password = password_edittext_register.text.toString()

        if(username.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(baseContext, "Please fill every text box",
                Toast.LENGTH_SHORT).show()
            return
        }

        Log.d("RegisterActivity", "Email is: " + email)
        Log.d("RegisterActivity", "password is: " + password)

        //Firebase authentication
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                //send verification email
                var emailVerified = FirebaseAuth.getInstance().currentUser?.isEmailVerified

                if(emailVerified == null)
                {
                    emailVerified = false
                }

                if(!emailVerified) {
                    val user = Firebase.auth.currentUser

                    user!!.sendEmailVerification()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Log.d("LoginActivity", "Email sent.")
                            }
                        }
                }

                uploadPhotoToFirebaseStorage()
                Log.d("Main", "Successfully created user with uid: ${it.result?.user?.uid}")

                //Go to Login
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)

                Toast.makeText(baseContext, "Verification email sent",
                    Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{
                Log.d("Main", "Failed to create user")
                Toast.makeText(baseContext, "${it.message}",
                    Toast.LENGTH_SHORT).show()
            }
    }

    private fun uploadPhotoToFirebaseStorage() {

        if(selectedPhotoUri == null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {

                Log.d("Register", "Image uploaded")
                ref.downloadUrl.addOnSuccessListener {

                    Log.d("RegisterActivity", "location $it")
                    saveUserToDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                Log.d("RegisterActivity", "User saved")
            }
    }

    private fun saveUserToDatabase(profileImageUrl: String) {

        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().uid?:""
        val user = hashMapOf(
            "id" to uid,
            "username" to username_edittext_register.text.toString(),
            "email" to email_edittext_register.text.toString(),
            "profileImageUrl" to profileImageUrl,
            "githubLink" to github_link_register.text.toString(),
            "sub" to "0"
        )

        db.collection("users")
            .add(user)
            .addOnSuccessListener { documentReference ->
                Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("Firestore", "Error adding document", e)
            }
    }
}
