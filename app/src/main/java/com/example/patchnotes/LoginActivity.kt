package com.example.patchnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.title = "Sign in"

        login_button.setOnClickListener {
            val email = email_edittext_login.text.toString()
            val password = password_edittext_login.text.toString()

            if(email.isEmpty() || password.isEmpty()) {
                Toast.makeText(baseContext, "Please fill every text box",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            Log.d("LoginActivity", "Email is: " + email)
            Log.d("LoginActivity", "password is: " + password)

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (!it.isSuccessful) return@addOnCompleteListener

                    //Email verification
                    var emailVerified = FirebaseAuth.getInstance().currentUser?.isEmailVerified

                    if(emailVerified == null)
                    {
                        emailVerified = false
                    }

                    if(!emailVerified) {
                        val user = Firebase.auth.currentUser

                        user!!.sendEmailVerification()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Log.d("LoginActivity", "Email sent.")
                                }
                            }
                        Toast.makeText(baseContext, "You need to verify your Email",
                            Toast.LENGTH_SHORT).show()
                        return@addOnCompleteListener
                    }
                    Toast.makeText(baseContext, "Login successful",
                        Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                .addOnFailureListener{
                    Log.d("Main", "Failed to log in")
                    Toast.makeText(baseContext, "${it.message}",
                        Toast.LENGTH_SHORT).show()
                }
        }

        go_to_register_textview.setOnClickListener {
            Log.d("LoginActivity", "Go to register activity")

            //Register Activity
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
}