package com.example.patchnotes

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.paypal.android.sdk.payments.PayPalConfiguration
import kotlinx.android.synthetic.main.activity_add_category.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class AddCategoryActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_category)

        supportActionBar?.title = "Add new project"

        category_add_new_category_button.setOnClickListener {
            uploadImageToFirebase()
        }

        select_category_image_button.setOnClickListener {
            Log.d("RegisterActivity", "Show photo")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    private var selectedimageUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            //selected photo
            Log.d("AddCategoryActivity", "Image was selected")

            selectedimageUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedimageUri)
            //val bitmapDrawable = BitmapDrawable(bitmap)
            //select_photo_button.setBackgroundDrawable(bitmapDrawable)

            select_category_image_imageview.setImageBitmap(bitmap)
            select_category_image_button.alpha = 0f
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun uploadImageToFirebase() {

        if(selectedimageUri == null) return
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedimageUri!!)
            .addOnSuccessListener {

                Log.d("Register", "Image uploaded")
                ref.downloadUrl.addOnSuccessListener {

                    Log.d("RegisterActivity", "location $it")
                    saveCategoryToDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                Log.d("RegisterActivity", "User saved")
            }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun saveCategoryToDatabase(ImageUrl: String) {

        val name = category_name_edittext.text.toString()
        val description = category_description_edittext.text.toString()

        if(name.isEmpty() || description.isEmpty()) {
            Toast.makeText(baseContext, "Please fill every text box",
                Toast.LENGTH_SHORT).show()
            return
        }

        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().uid?:""
        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val formatted = current.format(formatter)
        val category = hashMapOf(
            "user_id" to uid,
            "name" to name,
            "description" to description,
            "imageUrl" to ImageUrl,
            "date" to formatted
        )

        db.collection("categories")
            .add(category)
            .addOnSuccessListener { documentReference ->
                Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(baseContext, "$name has been added",
                    Toast.LENGTH_SHORT).show()
                val intent = Intent(this, ProfileActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
            .addOnFailureListener { e ->
                Log.w("Firestore", "Error adding new project", e)
            }
    }
}