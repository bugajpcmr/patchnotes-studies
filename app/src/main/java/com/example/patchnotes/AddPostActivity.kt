package com.example.patchnotes

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_add_category.*
import kotlinx.android.synthetic.main.activity_add_post.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddPostActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        supportActionBar?.title = "Add new post"

        val category_id: String = intent.getStringExtra("category_id")

        add_post_button.setOnClickListener {
            savePostToDatabase(category_id)
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun savePostToDatabase(category_id: String) {

        val title = add_title_post_edittext.text.toString()
        val description = add_description_post_edittext.text.toString()

        if(title.isEmpty() || description.isEmpty()) {
            Toast.makeText(baseContext, "Please fill every text box",
                Toast.LENGTH_SHORT).show()
            return
        }

        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val formatted = current.format(formatter)

        val db = Firebase.firestore
        //val uid = FirebaseAuth.getInstance().uid?:""
        val post = hashMapOf(
            "category_id" to category_id,
            "title" to title,
            "description" to description,
            "upvotes" to "100",
            "downvotes" to "20",
            "date" to formatted
        )

        db.collection("posts")
            .add(post)
            .addOnSuccessListener { documentReference ->
                Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(baseContext, "New post has been added",
                    Toast.LENGTH_SHORT).show()
                val intent = Intent(this, ProfileActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
            .addOnFailureListener { e ->
                Log.w("Firestore", "Error adding new project", e)
            }
    }
}