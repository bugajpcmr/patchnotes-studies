package com.example.patchnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_profile_settings.*

class ProfileSettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)

        change_password_button.setOnClickListener {
            val password1 = password_change1_edittext.text.toString()
            val password2 = password_change2_edittext.text.toString()


            if(password1.isEmpty() || password2.isEmpty() || (password1 != password2)) {
                Toast.makeText(baseContext, "Please fill all the text fields",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //val user = FirebaseAuth.getInstance().currentUser

            val user = Firebase.auth.currentUser
            user!!.updatePassword(password1)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        Log.d("ProfileSettingsActivity", "User password updated.")
                        Toast.makeText(baseContext, "Password updated",
                            Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, ProfileActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    }
                }
        }
    }
}