package com.example.patchnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.core.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.category_row.view.*
import kotlinx.android.synthetic.main.comment_row.view.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        supportActionBar?.title = "Patchnotes"

        val adapter = GroupAdapter<GroupieViewHolder>()
        recyclerview_categories.adapter = adapter

        go_to_profile_button.setOnClickListener {

            val intent = Intent(this, ProfileActivity::class.java)
            //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        go_to_favorite_button.setOnClickListener {
            val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener
            val intent = Intent(this, FavoritesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }


        fetchData()
    }

    private fun fetchData(){
        val db = Firebase.firestore
        db.collection("categories").orderBy("date", com.google.firebase.firestore.Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("HomeActivity", "${document.id} => ${document.data}")
                    val category = Category(
                        document.id,
                        document.getString("user_id")!!,
                        document.getString("name")!!,
                        document.getString("description")!!,
                        document.getString("imageUrl")!!,
                        document.getString("date")!!
                    )
                    if(category.id != "") {
                        adapter.add(CategoryItem(category))
                    }
                }

                adapter.setOnItemClickListener { item, view ->
                    val categoryItem = item as HomeActivity.CategoryItem
                    val intent = Intent(view.context, PostsActivity::class.java)
                    intent.putExtra("category_id", categoryItem.category.id)
                    intent.putExtra("user_id", categoryItem.category.user_id)
                    intent.putExtra("category_description", categoryItem.category.description)
                    intent.putExtra("category_imageurl", categoryItem.category.imageUrl)
                    intent.putExtra("category_name", categoryItem.category.name)
                    startActivity(intent)
                }

                recyclerview_categories.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("HomeActivity", "Error getting documents.", exception)
            }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId){

            R.id.menu_search ->{

                Log.d("HomeActivity", "SearchActivity")
                val intent = Intent(this, SearchActivity::class.java)
                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                //Toast.makeText(baseContext, "Search", Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.nav_menu_search, menu)

        return super.onCreateOptionsMenu(menu)
    }



    class CategoryItem(val category: Category): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.home_category_name_textview.text = category.name
            viewHolder.itemView.home_category_description_textview.text = category.description
            Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)
            viewHolder.itemView.add_to_favorites_button.setOnClickListener {
                val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener
                viewHolder.itemView.add_to_favorites_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
                addToFavorites(category.id)
            }
            viewHolder.itemView.profile_picture_category_imageview.setOnClickListener {
                val intent1 = Intent(viewHolder.itemView.context, UserProfileActivity::class.java)
                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent1.putExtra("user_profile_id", category.user_id)
                viewHolder.itemView.context.startActivity(intent1)
            }
        }
        override fun getLayout(): Int {
            return R.layout.category_row
        }

        fun addToFavorites(categoryId: String){

            val uid = FirebaseAuth.getInstance().uid?:""

            val db = Firebase.firestore
            val post = hashMapOf(
                "category_id" to categoryId,
                "user_id" to uid
            )

            db.collection("follows")
                .add(post)
                .addOnSuccessListener { documentReference ->
                    Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w("Firestore", "Error adding new project", e)
                }
        }
    }
}


class User(val id: String, val username: String, val email: String, val profileImageUrl: String, val sub: String, val githubLink: String)
class Category(val id: String, val user_id: String, val name: String, val description: String, val imageUrl: String, val date: String)
class Post(val id: String, val category_id: String, val title: String, val description: String, val upvotes: String, val downvotes: String, val date: String)
class Comment(val id: String, val post_id: String, val user_id: String, val content: String, val upvotes: Long, val date: String)