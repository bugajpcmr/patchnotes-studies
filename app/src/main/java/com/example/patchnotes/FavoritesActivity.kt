package com.example.patchnotes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_favorites.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.category_row.*
import kotlinx.android.synthetic.main.category_row.view.*
import kotlinx.android.synthetic.main.category_row.view.add_to_favorites_button

class FavoritesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)

        supportActionBar?.title = "Favorites"

        fetchData()
        val adapter = GroupAdapter<GroupieViewHolder>()
        recyclerview_favorite_categories.adapter = adapter

        go_to_home_button.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

    }


    private fun fetchData(){
        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().uid?:""

        db.collection("categories").orderBy("date", com.google.firebase.firestore.Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { result ->

                val adapter = GroupAdapter<GroupieViewHolder>()

                db.collection("follows").whereEqualTo("user_id", uid)
                    .get()
                for (document in result) {
                    Log.d("FavoritesActivity", "${document.id} => ${document.data}")
                    val category = Category(
                        document.id,
                        document.getString("user_id")!!,
                        document.getString("name")!!,
                        document.getString("description")!!,
                        document.getString("imageUrl")!!,
                        document.getString("date")!!
                    )
                    if(category.id != "") {
                        adapter.add(CategoryItem(category))
                    }
                }

                adapter.setOnItemClickListener { item, view ->
                    val categoryItem = item as FavoritesActivity.CategoryItem
                    val intent = Intent(view.context, PostsActivity::class.java)
                    intent.putExtra("category_id", categoryItem.category.id)
                    intent.putExtra("category_imageurl", categoryItem.category.imageUrl)
                    intent.putExtra("category_name", categoryItem.category.name)
                    startActivity(intent)
                }

                recyclerview_favorite_categories.adapter = adapter
                adapter.notifyDataSetChanged()
                //add_to_favorites_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
            }
            .addOnFailureListener { exception ->
                Log.w("HomeActivity", "Error getting documents.", exception)
            }
    }

    class CategoryItem(val category: Category): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.home_category_name_textview.text = category.name
            viewHolder.itemView.home_category_description_textview.text = category.description
            Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)
            viewHolder.itemView.add_to_favorites_button.setOnClickListener {
                viewHolder.itemView.add_to_favorites_button.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
                removeFromFavorites(category.id)
                val adapter = GroupAdapter<GroupieViewHolder>()
                adapter.notifyDataSetChanged()
            }
            viewHolder.itemView.add_to_favorites_button.isVisible = false
        }
        override fun getLayout(): Int {
            return R.layout.category_row
        }

        private fun removeFromFavorites(categoryId: String){

            val uid = FirebaseAuth.getInstance().uid?:""
            val db = Firebase.firestore
            db.collection("follows").whereEqualTo("user_id", uid)
                .get()
                .addOnSuccessListener { result ->

                    db.collection("follows").whereEqualTo("category_id", categoryId)
                        .get()

                    for (document in result) {
                        Log.d("HomeActivity", "${document.id} => ${document.data}")
                        db.collection("follows").document(document.id)
                            .delete()
                            .addOnSuccessListener {
                                Log.d("removeFollow", "DocumentSnapshot successfully deleted!") }
                            .addOnFailureListener { e -> Log.w("removeFollow", "Error deleting document", e) }
                    }

                }
                .addOnFailureListener { exception ->
                    Log.w("HomeActivity", "Error getting documents.", exception)
                }
        }
    }


}