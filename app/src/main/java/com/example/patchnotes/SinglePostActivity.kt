package com.example.patchnotes

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_single_post.*
import kotlinx.android.synthetic.main.comment_row.*
import kotlinx.android.synthetic.main.comment_row.view.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

var post_Id: String = ""
var post_Title: String = ""
var post_Description: String = ""
var post_Date: String = ""

class SinglePostActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_post)

        supportActionBar?.title = "Full Post"

        //singlepost_post_description_textview.isVisible = false

        post_Id = intent.getStringExtra("post_id")!!
        post_Title = intent.getStringExtra("post_title")!!
        post_Description = intent.getStringExtra("post_description")!!
        post_Date = intent.getStringExtra("post_date")!!

        val adapter = GroupAdapter<GroupieViewHolder>()
        singlepost_comments_recyclerview.adapter = adapter

        singlepost_post_title_textview.text = post_Title
        singlepost_post_description_textview.text = post_Description
        single_post_date_textview.text = post_Date
        //Picasso.get().load(category.imageUrl).into(viewHolder.itemView.category_home_imageview)

        fetchCommentData(post_Id)

        top_post_info_layout.setOnClickListener {
            singlepost_post_description_textview.isVisible = !singlepost_post_description_textview.isVisible

        }

        add_comment_button.setOnClickListener {
            hideKeyboard()
            val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener
            saveCommentToDatabase(post_Id)
            adapter.clear();
            add_comment_edittext.text.clear()
            fetchCommentData(post_Id)
        }
    }
    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun saveCommentToDatabase(post_id: String) {

        val content = add_comment_edittext.text.toString()

        if(content.isEmpty()) {
            Toast.makeText(baseContext, "Please fill every text box",
                Toast.LENGTH_SHORT).show()
            return
        }

        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val formatted = current.format(formatter)

        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().uid?:""
        val comment = hashMapOf(
            "post_id" to post_id,
            "user_id" to uid,
            "content" to content,
            "upvotes" to 0,
            "date" to formatted
        )

        db.collection("comments")
            .add(comment)
            .addOnSuccessListener { documentReference ->
                Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(baseContext, "New comment has been added",
                    Toast.LENGTH_SHORT).show()
                //val intent = Intent(this, ProfileActivity::class.java)
                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                //startActivity(intent)
            }
            .addOnFailureListener { e ->
                Log.w("Firestore", "Error adding new project", e)
            }
    }

    private fun fetchCommentData(post_id: String){
        val db = Firebase.firestore

        db.collection("comments")
            .whereEqualTo("post_id", post_id)
            .get()
            .addOnSuccessListener { result ->

                db.collection("comments")
                    .orderBy("upvotes", Query.Direction.DESCENDING)
                    .get()
                val adapter = GroupAdapter<GroupieViewHolder>()

                for (document in result) {
                    Log.d("SinglePostActivity", "${document.id} => ${document.data}")
                    val comment = Comment(
                        document.id,
                        document.getString("post_id")!!,
                        document.getString("user_id")!!,
                        document.getString("content")!!,
                        document.getLong("upvotes")!!,
                        document.getString("date")!!
                    )
                    if(comment.id != "") {
                        adapter.add(CommentItem(comment, document.id))
                    }
                }


                adapter.setOnItemClickListener { item, view ->
                    view.singlepost_comment_content_textview.isVisible =
                        view.singlepost_comment_content_textview.isVisible != true

                }

                singlepost_comments_recyclerview.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.w("SinglePostActivity", "Error getting documents.", exception)
            }
    }

    class CommentItem(val comment: Comment, val category_document_id: String): Item<GroupieViewHolder>() {

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {

            val db = Firebase.firestore
            db.collection("users").whereEqualTo("id", comment.user_id)
                .get()
                .addOnSuccessListener { result ->

                    val adapter = GroupAdapter<GroupieViewHolder>()

                    for (document in result) {
                        Log.d("SinglePostActivity", "${document.id} => ${document.data}")
                        val user = User(
                            document.getString("id")!!,
                            document.getString("username")!!,
                            document.getString("email")!!,
                            document.getString("profileImageUrl")!!,
                            document.getString("sub")!!,
                            document.getString("githubLink")!!
                        )
                        if(user.id != "") {
                            viewHolder.itemView.singlepost_comment_content_textview.isVisible =
                                comment.upvotes >= 0
                            viewHolder.itemView.singlepost_comment_content_textview.text = comment.content
                            viewHolder.itemView.singlepost_comment_upvote_count_textview.text = comment.upvotes.toString()
                            viewHolder.itemView.singlepost_comment_date_textview.text = comment.date
                            viewHolder.itemView.singlepost_comment_username_textview.text = user.username
                            Picasso.get().load(user.profileImageUrl).into(viewHolder.itemView.singlepost_comment_profile_picture_imageview)

                            //go to user profile
                            viewHolder.itemView.singlepost_comment_profile_picture_imageview.setOnClickListener {
                                val intent1 = Intent(viewHolder.itemView.context, UserProfileActivity::class.java)
                                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent1.putExtra("user_profile_id", user.id)
                                viewHolder.itemView.context.startActivity(intent1)
                            }

                            val uid = FirebaseAuth.getInstance().uid
                            if (uid == comment.user_id) {
                                viewHolder.itemView.singlepost_comment_delete_button.isVisible = true
                            }
                            viewHolder.itemView.singlepost_comment_delete_button.setOnClickListener {
                                //val db = Firebase.firestore

                                val builder = AlertDialog.Builder(viewHolder.itemView.context)

                                builder.setTitle("Delete")
                                builder.setMessage("Are you sure you want to delete this comment?")

                                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                                    db.collection("comments").document(comment.id)
                                        .delete()
                                        .addOnSuccessListener { Log.d("SinglePostActivity", "DocumentSnapshot successfully deleted!") }
                                        .addOnFailureListener { e -> Log.w("SinglePostActivity", "Error deleting document", e) }
                                    Toast.makeText(viewHolder.itemView.context, "Comment deleted", Toast.LENGTH_SHORT).show()
                                    //restart activity/recyclerview
                                    val intent = Intent(viewHolder.itemView.context, SinglePostActivity::class.java)
                                    intent.putExtra("post_id", post_Id)
                                    intent.putExtra("post_title", post_Title)
                                    intent.putExtra("post_description", post_Description)
                                    intent.putExtra("post_date", post_Date)
                                    viewHolder.itemView.context.startActivity(intent)
                                })

                                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                                    // Do nothing
                                    dialog.dismiss()
                                })

                                val alert = builder.create()
                                alert.show()


                            }
                            viewHolder.itemView.singlepost_comment_upvote_button.setOnClickListener {
                                val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener
                                var up_votes: Long = comment.upvotes
                                up_votes++
                                viewHolder.itemView.singlepost_comment_upvote_count_textview.text = up_votes.toString()
                                db.collection("comments").document(category_document_id)
                                    .update("upvotes", up_votes)
                                adapter.notifyDataSetChanged()
                            }
                            viewHolder.itemView.singlepost_comment_downvote_button.setOnClickListener {
                                val uid = FirebaseAuth.getInstance().uid ?: return@setOnClickListener
                                var down_votes: Long = comment.upvotes
                                down_votes--
                                viewHolder.itemView.singlepost_comment_upvote_count_textview.text = down_votes.toString()
                                db.collection("comments").document(category_document_id)
                                    .update("upvotes", down_votes)
                                adapter.notifyDataSetChanged()
                            }
                            viewHolder.itemView.singlepost_comment_report_button.setOnClickListener {


                                val builder = AlertDialog.Builder(viewHolder.itemView.context)

                                builder.setTitle("Report")
                                builder.setMessage("Are you sure you want to report this comment?")

                                builder.setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                                    val uid = FirebaseAuth.getInstance().uid ?: return@OnClickListener
                                    //val uid = FirebaseAuth.getInstance().uid?:""
                                    val reportComment = hashMapOf(
                                        "comment_id" to comment.id,
                                        "user_id" to comment.user_id
                                    )

                                    db.collection("reported_comments")
                                        .add(reportComment)
                                        .addOnSuccessListener { documentReference ->
                                            Log.d("Firestore", "DocumentSnapshot added with ID: ${documentReference.id}")
                                            Toast.makeText(viewHolder.itemView.context, "Comment reported", Toast.LENGTH_SHORT).show()
                                        }
                                        .addOnFailureListener { e ->
                                            Log.w("Firestore", "Error adding new project", e)
                                        }
                                })

                                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->
                                    // Do nothing
                                    dialog.dismiss()
                                })

                                val alert = builder.create()
                                alert.show()



                            }
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("SinglePostActivity", "Error getting documents.", exception)
                }



        }
        override fun getLayout(): Int {
            return R.layout.comment_row
        }
    }
}